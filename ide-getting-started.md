Download and install the Arduino IDE:

* https://www.arduino.cc/en/software

Add the following URLs to the Additional Boards Manager (File -> Preferences -> Settings):

* http://arduino.esp8266.com/stable/package_esp8266com_index.json
* https://raw.githubusercontent.com/sparkfun/Arduino_Boards/master/IDE_Board_Manager/package_sparkfun_index.json
* https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json

Then make sure the following board packages are installed (Tools -> Board: whatever -> Boards manager):

* Arduino AVR Boards (arduino)
* esp8266 (esp8266)
* SparkFun AVR Boards (SparkFun)
* Raspberry Pi Pico/RP2040

Then make sure the following libraries are installed (Tools -> Manage Libraries):

* FastLED
* Servo
* LowPower_LowPowerLab

For more information, check out these links:

* https://learn.sparkfun.com/pages/CustomBoardsArduino
* https://github.com/sparkfun/Arduino_Boards
* https://www.tomshardware.com/how-to/program-raspberry-pi-pico-with-arduino-ide
