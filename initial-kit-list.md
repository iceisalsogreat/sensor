# Initial kit list

I often buy a few of many items so that I have spares lying around.

Raspberry Pi Pico (Headered)

* https://shop.pimoroni.com/products/raspberry-pi-pico?variant=32402092326995

The Pico are currently in stock at Pimoroni.  The prices for the SparkFun Pro Micro are now crazy,
so I suggest we avoid this alternative.  The STM32-based boards probably don't provide many
additional niceties, so let's avoid those.

Wemos D1 Mini (WiFi plus small microcontroller):

* https://www.ebay.co.uk/itm/203579081685

MPU-9250 MEMS gyroscope / accelerometer / magnetometer (this one has the INT pin, which we want)

* https://www.ebay.co.uk/itm/163006980817

Logic level shifter (probably go for two, just in case)

* https://shop.pimoroni.com/products/logic-level-shifter-module

Breadboard

* https://shop.pimoroni.com/products/solderless-breadboard-830-point

Breadboard cables (which include a couple of mini breadboards)

* https://shop.pimoroni.com/products/maker-essentials-mini-breadboards-jumper-jerky

LiPO (for safety)

* https://shop.pimoroni.com/products/lipo-battery-pack?variant=20429082247

LiPO charger board

* https://shop.pimoroni.com/products/powerboost-1000-charger-rechargeable-5v-lipo-usb-boost-1a-1000c

Also:

* Soldering iron
* Solder (lead-based is preferable, particularly for beginners, no restriction for hobbyists)
