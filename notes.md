# General notes on requirements

So, thought experiment: Would it be possible to design apparatus to measure:

* changes in magnetic field;
* sudden/localised drop in temperature;
* sudden/localised changes in humidity;
* Low-frequency sound waves (infrasound < 20Hz)?

# Possible sensors

* Three axis magnetometer
 * 9250 (https://invensense.tdk.com/products/motion-tracking/9-axis/mpu-9250/)
 * 9250 breakout board (https://www.ebay.co.uk/itm/163006980817)
* Three axis accelerometer
 * 9250 (https://invensense.tdk.com/products/motion-tracking/9-axis/mpu-9250/)
 * 9250 breakout board (https://www.ebay.co.uk/itm/163006980817)
* Three axis gyroscope
  * 9250 (https://invensense.tdk.com/products/motion-tracking/9-axis/mpu-9250/)
 * 9250 breakout board (https://www.ebay.co.uk/itm/163006980817)
* Temperature reading (probably want something like 0.1 degree precision)
 * 100k thermistor would be the easiest from a hardware perspective
 * BMP280 https://www.ebay.co.uk/itm/272379854598
 * DS18B20+ https://uk.farnell.com/maxim-integrated-products/ds18b20/temperature-sensor/dp/2515553
* Barometric pressure
 * BMP280 https://www.ebay.co.uk/itm/272379854598
* Hygrometer
 * https://www.ebay.co.uk/itm/174288798703?epid=7012438731
 * Not sure how well these work in air
* Infrasound
 * Possibly use the accelerometer on the 9250, though not sure how
 * I can't imagine standard microsphones work well with infrasound (e.g. https://www.adafruit.com/product/2716 starts at 100Hz)

# Possible microcontrollers

Microcontroller to collect data and transmit.  We don't yet know if these are powerful enough.

* Many options, probably £5-£10 each:
 * Sparkfun Pro Micro
 * Raspberry Pi Pico
 * STM32 family

# WiFi

WiFi to transmit data to a phone or a laptop:
* ESP8266 or similar (£5-£10)
 * e.g. https://www.ebay.co.uk/itm/203579081685

# Other hardware

* LiPO
 * https://shop.pimoroni.com/products/lipo-battery-pack?variant=20429082247
* LiPO charger board
 * https://shop.pimoroni.com/products/powerboost-1000-charger-rechargeable-5v-lipo-usb-boost-1a-1000c
* Battery holder?
* VU-meters
 * e.g. https://www.ebay.co.uk/itm/284311101275

# Additional notes

* Go for USB-C instead of other connectors
* I'm now thinking that going for LiPO and a good LiPO charger will be safer than 18650 Lithium Ion and TP4056
* Could possibly even get boards designed (and mostly built) via e.g. JLCPCB.
